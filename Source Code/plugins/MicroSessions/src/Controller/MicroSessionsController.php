<?php
declare(strict_types=1);

namespace MicroSessions\Controller;
use MicroSessions\Controller\AppController;
use Cake\ORM\TableRegistry;


/**
 * MicroSessions Controller
 *
 * 
 * @method \MicroSessions\Model\Entity\MicroSession[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MicroSessionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
       /* $query = $this->MicroSessions;
        $query->contain(['GradingTypes', 'Subjects']);
        $query->where(['MicroSessions.user_id' => $this->getRequest()->getAttribute('identity')->id]);
        $options['order'] = ['MicroSessions.id' => 'DESC'];
        $options['limit'] = \Cake\Core\Configure::read('Setting.ADMIN_PAGE_LIMIT');
        $this->paginate = $options;
        $microSessions = $this->paginate($query);*/
		
		$microSessions = $this->paginate($this->MicroSessions);
		
		

        $this->set(compact('microSessions'));
    }

    /**
     * View method
     *
     * @param string|null $id Micro Session id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /*public function view($id = null)
    {
        $microSession = $this->MicroSessions->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('microSession'));
    }*/

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {
          if($id){
            $microSession = $this->MicroSessions->get($id, [
                'contain' => [],
            ]);
        }else{
            $microSession = $this->MicroSessions->newEmptyEntity();
        }

        //$microSession = $this->MicroSessions->newEmptyEntity();
       if ($this->request->is(['patch', 'post', 'put'])) {
            $microSession = $this->MicroSessions->patchEntity($microSession, $this->request->getData());
			$microSession->user_id = $this->getRequest()->getAttribute('identity')->id;
           /*echo "<pre>";
           print_r($microSession);
            die(); */
			//dd($microSession); die;
            if ($this->MicroSessions->save($microSession)) {
                $this->Flash->success(__('The micro session has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The micro session could not be saved. Please, try again.'));
        }
		//$listings = $this->MicroSessions->Listings->find('list', ['limit' => 200]);
         //$parentMicroSessions = $this->MicroSessions->parentMicroSessions->find('list', ['limit' => 200]);
         $gradingTypes = $this->MicroSessions->GradingTypes->find('list', ['limit' => 200]);
		
		$boards = $this->MicroSessions->Boards->find('list', ['limit' => 200]);
        $subjects = $this->MicroSessions->Subjects->find('list', ['limit' => 200]);
		$this->set(compact('microSession',  'gradingTypes', 'boards', 'subjects'));
        
    }

    /**
     * Edit method
     *
     * @param string|null $id Micro Session id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $microSession = $this->MicroSessions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $microSession = $this->MicroSessions->patchEntity($microSession, $this->request->getData());
            if ($this->MicroSessions->save($microSession)) {
                $this->Flash->success(__('The micro session has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The micro session could not be saved. Please, try again.'));
        }
        $this->set(compact('microSession'));
    }


/**
     * View method
     *
     * @param string|null $id Course id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
       
		$query = $this->MicroSessions->find()->where(['MicroSessions.id' => $id]);
        //$query->contain(['ParentCourses', 'GradingTypes', 'ChildCourses', 'Subjects', 'Boards', 'CourseChapters', 'Reviews']); 
        //$query->select(['total_chapters' => $query->func()->count('CourseChapters.id'), 'avg_rating' => $query->func()->avg('rating')]);
        //$query->leftJoinWith('CourseChapters', function($q){
          //  return $q;
        //});
        //$query->leftJoinWith('Reviews', function($q){
          //  return $q;
        //}); 
        $query->group(['MicroSessions.id'])->enableAutoFields(true);

        /*$query->contain(['Users' => function($q){
            return $q->leftJoinWith('Reviews', function($q){
            return $q;
        })
        ->group(['Users.id'])
        ->select(['Users.id', 'teacher_rating' => $q->func()->avg('Reviews.rating'), 'total_rating' => $q->func()->count('Reviews.id')])->contain(['UserProfiles', 'Reviews'])->enableAutoFields(true);
        }]);*/ 
 
        //dump($this->request->getAttribute('identity'));
       /* if($this->request->getAttribute('identity')){
            $query->contain(['OrderCourses.Orders' => function($q){
                    return $q->where(['Orders.transaction_status' => 1, 'Orders.user_id' => $this->request->getAttribute('identity')->id]);
            }]);
        }*/

        $microSession = $query->firstOrFail();
      //  dd($course);
        $totalCourses = $this->MicroSessions->find()->where(['MicroSessions.user_id' => $microSession->user_id])->count();
        $this->set(compact('microSession', 'totalCourses'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Micro Session id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
      
       $microSession = $this->MicroSessions->get($id);
       $this->MicroSessions->delete($microSession);
        // if ($this->MicroSessions->delete($microSession)) {
        //     $this->Flash->success(__('The micro session has been deleted.'));
        //     print_r($microSession);
        //   die();
        // } else {
        //     $this->Flash->error(__('The micro session could not be deleted. Please, try again.'));
        // }

        return $this->redirect(['action' => 'index']);
    }
}
