<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $microSessionChapter
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $microSessionChapter->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $microSessionChapter->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Micro Session Chapters'), ['action' => 'index'], ['class' => 'btn mybtn btn-block']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="microSessionChapters form content">
            <?= $this->Form->create($microSessionChapter) ?>
            <fieldset>
                <legend><?= __('Edit Micro Session Chapter') ?></legend>
                <?php
                    echo $this->Form->control('micro_session_id');
                    echo $this->Form->control('title');
                    echo $this->Form->control('video_url');
                    echo $this->Form->control('chapter_file');
                    echo $this->Form->control('chapter_file_dir');
                    echo $this->Form->control('chapter_file_size');
                    echo $this->Form->control('chapter_file_type');
                    echo $this->Form->control('short_description');
                    echo $this->Form->control('description');
                    echo $this->Form->control('position');
                    echo $this->Form->control('is_free');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
