<?php
/**
 * Copyright 2010 - 2019, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2018, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

use Cake\Core\Configure;

?>

 <div class="SLoginArea">
        <div class="container">
            <div class="col-md-12">
                <div class="col-md-8 col-md-offset-2 SLbox">
                    <div class="col-md-12 LoginTop">
                        <?= $this->Html->image("LogoIcon.png") ?>
                        <h2>Student Registration</h2>
                    </div>
                    <div class="col-md-12">
                        <?= $this->Flash->render() ?>
                        <?= $this->Flash->render('auth') ?>
                        <?= $this->Form->create($user, ['templates' => 'bootstrap_validation']) ?>
                                <?php //dump($user->getErrors()); ?>
                                <div class="row">
                            <div class="col-md-6 form-group">
                                <?= $this->Form->control('first_name', ['label' => __d('cake_d_c/users', 'First Name'), 'class' => 'form-control mytbox', 'placeholder' => 'First Name']); ?>
                            </div>
                            <div class="col-md-6 form-group">
                                <?= $this->Form->control('last_name', ['label' => __d('cake_d_c/users', 'Last name'), 'class' => 'form-control mytbox', 'placeholder' => 'Last Name']); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <?= $this->Form->control('email', ['label' => __d('cake_d_c/users', 'Email'), 'class' => 'form-control mytbox', 'placeholder' => 'Your Email']); ?>
                            </div>
                            <div class="col-md-6 form-group">
                                <?= $this->Form->control('username', ['label' => __d('cake_d_c/users', 'Username'), 'class' => 'form-control mytbox', 'placeholder' => 'Your Username']); ?>
                            </div>
                            </div>
                           
                            <div class="row">
                                 <div class="col-md-6 form-group">
                                    <?= $this->Form->control('user_profile.mobile', ['label' => __d('cake_d_c/users', 'Contact Number'), 'class' => 'form-control mytbox', 'placeholder' => 'Your Contact Number']); ?>
                                </div>
                                <div class="col-md-6 form-group">
                                <?= $this->Form->control('user_profile.location_id', ['options' => $locations, 'class' => 'form-control mytbox', 'empty' => 'Select Your State', 'label' =>  __d('cake_d_c/users', 'Location')]); ?>    
                             
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6 form-group">
                                <?= $this->Form->control('password', ['label' => __d('cake_d_c/users', 'Password'), 'class' => 'form-control mytbox', 'placeholder' => 'Password']); ?>
                            </div>
                            <div class="col-md-6 form-group">
                                <?= $this->Form->control('password_confirm', [
                                    'required' => true,
                                    'type' => 'password',
                                    'label' => __d('cake_d_c/users', 'Confirm password'),
                                    'class' => 'form-control mytbox', 
                                    'placeholder' => 'Confirm password'
                                ]); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 mt20 mb40">
                                <?= $this->Form->button(__d('cake_d_c/users', 'Submit'), ['class'=>'btn btn-block mybtn']) ?>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                    <div class="col-md-12 Sslogin">
                        <?= implode(' ', $this->User->socialLoginList()); ?>
                    </div>
                    <div class="col-md-12 nopadding SwpipeLogin">
                        <?php echo $this->Html->link('Already have Account? Login here', ['action' => 'login']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php /*
<div class="users form large-10 medium-9 columns">
    <?= $this->Form->create($user); ?>
    <fieldset>
        <legend><?= __d('cake_d_c/users', 'Add User') ?></legend>
        <?php
        echo $this->Form->control('username', ['label' => __d('cake_d_c/users', 'Username')]);
        echo $this->Form->control('email', ['label' => __d('cake_d_c/users', 'Email')]);
        echo $this->Form->control('password', ['label' => __d('cake_d_c/users', 'Password')]);
        echo $this->Form->control('password_confirm', [
            'required' => true,
            'type' => 'password',
            'label' => __d('cake_d_c/users', 'Confirm password')
        ]);
        echo $this->Form->control('first_name', ['label' => __d('cake_d_c/users', 'First name')]);
        echo $this->Form->control('last_name', ['label' => __d('cake_d_c/users', 'Last name')]);
        if (Configure::read('Users.Tos.required')) {
            echo $this->Form->control('tos', ['type' => 'checkbox', 'label' => __d('cake_d_c/users', 'Accept TOS conditions?'), 'required' => true]);
        }
        if (Configure::read('Users.reCaptcha.registration')) {
            echo $this->User->addReCaptcha();
        }
        ?>
    </fieldset>
    <?= $this->Form->button(__d('cake_d_c/users', 'Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<?php */ ?>
<?php 
echo $this->Html->css(['/assets/plugins/dropify-master/dist/css/dropify.min.css'],['block' => true]);
echo $this->Html->script('https://use.fontawesome.com/fbf7ab0391.js',['block' => true]);
 ?>