<?php
/**
 * Copyright 2010 - 2019, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2018, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

use Cake\Core\Configure;

?>

<div class="SLoginArea">
        <div class="container">
            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-4 SLbox">
                    <div class="col-md-12 LoginTop">
                        <?= $this->Html->image("LogoIcon.png") ?>
                        <h2>Login</h2>
                    </div>
                    <div class="col-md-12">
                        <?= $this->Flash->render() ?>
                        <?= $this->Flash->render('auth') ?>
                        <?= $this->Form->create(null, ['templates' => [
                            'inputContainer' => '<div class="input {{type}}{{required}}"><div class="form-group">{{content}}</div></div>', 
                            'input' => '<input type="{{type}}" name="{{name}}"{{attrs}}/><span class="invalid-feedback" role="alert"></span>',
                            'label' => '<label{{attrs}} class="control-label">{{text}}</label>',
                                'formGroup' => '{{label}}{{input}}{{error}}',
                                ]]) ?>
                            <div class="form-group">
                                <?= $this->Form->control('username', ['label' => __d('cake_d_c/users', 'Username / Email'),'class' => 'form-control mytbox','placeholder' => 'Username / Email' , 'required' => true]) ?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('password', ['label' => __d('cake_d_c/users', 'Password'),'placeholder' => 'Password','class' => 'form-control mytbox', 'required' => true]) ?>
                            </div>
                            <?= $this->Form->button(__d('cake_d_c/users', 'LOGIN'), ['class' => 'btn btn-block mybtn']); ?>
                        <?= $this->Form->end() ?>
                    </div>
                    <div class="col-md-12 LoginFS">
                        <?php
                        $registrationActive = Configure::read('Users.Registration.active');
                        
                        if (Configure::read('Users.Email.required')) {
                            if ($registrationActive) {
                                echo '';
                            }
                            echo $this->Html->link(__d('cake_d_c/users', 'Forgot Password?'), ['action' => 'requestResetPassword']);
                        }
                        ?>
                    </div>
                    <div class="col-md-12 nopadding Sslogin">
                        <?= implode(' ', $this->User->socialLoginList()); ?>
                        <!-- <div class="col-md-12"> 
                            <a href="#">
                                <img src="Include/img/SocialLogin1.png" /></a>
                        </div>
                        <div class="col-md-12">
                            <a href="#">
                                <img src="Include/img/SocialLogin2.png" /></a>
                        </div> -->
                    </div>
                    <div class="col-md-12 nopadding SwpipeLogin">
                        <?php
                        if ($registrationActive) {
                            echo $this->Html->link(__d('cake_d_c/users', 'Dont have Account? Sign Up here'), ['action' => 'register']);
                        }
                         ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php /*
<div class="users form">
    <?= $this->Flash->render('auth') ?>
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __d('cake_d_c/users', 'Please enter your username and password') ?></legend>
        <?= $this->Form->control('username', ['label' => __d('cake_d_c/users', 'Username'), 'required' => true]) ?>
        <?= $this->Form->control('password', ['label' => __d('cake_d_c/users', 'Password'), 'required' => true]) ?>
        <?php
        if (Configure::read('Users.reCaptcha.login')) {
            echo $this->User->addReCaptcha();
        }
        if (Configure::read('Users.RememberMe.active')) {
            echo $this->Form->control(Configure::read('Users.Key.Data.rememberMe'), [
                'type' => 'checkbox',
                'label' => __d('cake_d_c/users', 'Remember me'),
                'checked' => Configure::read('Users.RememberMe.checked')
            ]);
        }
        ?>
        <?php
        $registrationActive = Configure::read('Users.Registration.active');
        if ($registrationActive) {
            echo $this->Html->link(__d('cake_d_c/users', 'Register'), ['action' => 'register']);
        }
        if (Configure::read('Users.Email.required')) {
            if ($registrationActive) {
                echo ' | ';
            }
            echo $this->Html->link(__d('cake_d_c/users', 'Reset Password'), ['action' => 'requestResetPassword']);
        }
        ?>
    </fieldset>
    <?= implode(' ', $this->User->socialLoginList()); ?>
    <?= $this->Form->button(__d('cake_d_c/users', 'Login')); ?>
    <?= $this->Form->end() ?>
</div>
*/ ?>

<?php 
echo $this->Html->css(['/assets/plugins/dropify-master/dist/css/dropify.min.css'],['block' => true]);
echo $this->Html->script('https://use.fontawesome.com/fbf7ab0391.js',['block' => true]);
 ?>